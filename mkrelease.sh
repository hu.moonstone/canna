#!/bin/sh
# $Id: mkrelease.sh,v 1.7 2004/04/25 14:16:48 aida_s Exp $
set -e
set -x
cp Canna.conf.dist Canna.conf
autoconf
autoheader
rm -rf autom4te.cache

wget -O config.guess 'https://git.savannah.gnu.org/gitweb/?p=config.git;a=blob_plain;f=config.guess;hb=HEAD'
wget -O config.sub 'https://git.savannah.gnu.org/gitweb/?p=config.git;a=blob_plain;f=config.sub;hb=HEAD'

cd canuum
autoconf
autoheader
rm -rf autom4te.cache
